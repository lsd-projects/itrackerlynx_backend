import {
  getAllUsers,
  authUser,
  getUser
} from "./models/controlers/UserController";

import{
  getAllTeams,
  getAllTeamsOf,
  addUserToTeam
} from "./models/controlers/TeamController"

/**
 * All application routes.
 */
export const AppRoutes = [
    {
        path: "/api/users",
        method: "get",
        action: getAllUsers
    },
    {
        path: "/api/user",
        method: "post",
        action: getUser
    },
    {
        path: "/api/teams",
        method: "get",
        action: getAllTeams
    },
    {
        path: "/api/teams",
        method: "post",
        action: getAllTeamsOf
    },
    {
        path: "/api/team/adduser",
        method: "post",
        action: addUserToTeam
    },
    {
        path: "/api/auth",
        method: "post",
        action: authUser
    }
];
