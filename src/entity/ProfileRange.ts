import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
} from "typeorm";
import {User} from "./User";

@Entity()
export class ProfileRange{

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  isAdmin: boolean;

  @Column()
  abmUser: boolean;

  @Column()
  abmTree: boolean;

  @Column()
  createTicket: boolean;

  @Column()
  inbox: boolean;

  @Column()
  pickTicket: boolean;

  @Column()
  report: boolean;

  @Column()
  massive: boolean;

  @OneToMany(type => User, user => user.profileTickt)
  users: User[];
}
