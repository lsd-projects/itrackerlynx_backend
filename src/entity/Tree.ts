import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  ManyToMany,
  JoinTable,
} from "typeorm";
import {Question} from "./Question";
import {Team} from "./Team";

@Entity()
export class Tree{

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  detail: string;

  @Column()
  isClose: boolean;

  @ManyToOne(type => Question, question => question.trees)
  question: Question;

  @ManyToMany(type => Team, team => team.trees)
  @JoinTable()
  teams: Team[];
}
