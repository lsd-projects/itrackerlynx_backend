import {
  Entity,
  PrimaryColumn,
  Column,
  OneToMany,
  ManyToOne,
  ManyToMany,
  JoinTable
} from "typeorm";
import {Interaction} from "./Interaction";
import {ProfileRange} from "./ProfileRange";
import {Team} from "./Team";
import {Ticket} from "./Ticket";

@Entity()
export class User {

    @PrimaryColumn()
    user: string;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    email: string;

    @Column()
    phone: string;

    @Column()
    movil: string;

    @Column()
    avatar: string;

    @Column({default: true})
    isActive: boolean;

    @OneToMany(type => Ticket, ticket => ticket.creator)
    ticketsCreated: Ticket[];

    @OneToMany(type => Ticket, ticket => ticket.userInCharge)
    ticketsTaken: Ticket[];

    @ManyToOne(type => ProfileRange, profile => profile.users)
    profileTickt: ProfileRange;

    @ManyToMany(type => Team, team => team.members)
    @JoinTable()
    teams: Team[];

    @ManyToOne(type => Interaction, interaction => interaction.user)
    interactionsCreate: Interaction;
}
