import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToMany,
  JoinTable,
} from "typeorm";
import {Option} from "./Option";
import {Tree} from "./Tree";

@Entity()
export class Question{

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  detail: string;

  @Column()
  isClose: boolean;

  @ManyToMany(type => Option, option => option.question)
  options: Option[];

  @OneToMany(type => Tree, tree => tree.question)
  trees: Tree[];

  @OneToMany(type => Option, option => option.questionOut)
  referToMe: Question;
}
