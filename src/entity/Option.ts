import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  ManyToMany,
  JoinTable
} from "typeorm";
import {Question} from "./Question";

@Entity()
export class Option{

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  detail: string;

  @Column()
  isClose: boolean;

  @Column()
  form: string;

  @ManyToMany(type => Question, quiestion => quiestion.options)
  @JoinTable()
  question: Question;

  @ManyToOne(type => Question, quest => quest.referToMe)
  questionOut: Question;
}
