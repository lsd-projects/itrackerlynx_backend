import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
} from "typeorm";
import {Ticket} from "./Ticket";
import {User} from "./User";

@Entity()
export class Interaction{

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  formComplete: string;

  @CreateDateColumn()
  dateCreate: Date;

  @ManyToOne(type => Ticket, ticket => ticket.interactions)
  ticket: Ticket;

  @ManyToOne(type => User, user => user.interactionsCreate)
  user: User;
}
