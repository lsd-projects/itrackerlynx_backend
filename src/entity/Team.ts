import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToMany,
  JoinTable
} from "typeorm";
import {Button} from "./Button";
import {Ticket} from "./Ticket";
import {Tree} from "./Tree";
import {User} from "./User";

@Entity()
export class Team{

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column()
  isActive: boolean;

  @Column()
  timeToReopen:number;

  @OneToMany(type => Ticket, ticket => ticket.teamInCharge)
  ticketsRecieved: Ticket[];

  @ManyToMany(type => User, user => user.teams)
  members: User[];

  @ManyToMany(type => Button, button => button.teams)
  @JoinTable()
  buttons: Button[];

  @ManyToMany(type => Tree, tree => tree.teams)
  @JoinTable()
  trees: Tree[];
}
