import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  OneToMany,
}from "typeorm";
import {Interaction} from "./Interaction";
import {Team} from "./Team";
import {User} from "./User";
import {Priority} from "../models/ticket/Priority";

@Entity()
export class Ticket{

  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => User, user => user.ticketsCreated)
  creator: User;

  @Column()
  treeSelected: string;

  @Column()
  formComplete: string;

  @Column({
        type: "enum",
        enum: Priority,
        default: Priority.MEDIA
    })
  priority: Priority;

  @ManyToOne(type => Team, team => team.ticketsRecieved)
  teamInCharge: Team;

  @ManyToOne(type => User, user => user.ticketsTaken)
  userInCharge: User;

  @CreateDateColumn()
  dateCreate: Date;

  @Column()
  dateClose: Date;

  @OneToMany(type => Interaction, interaction => interaction.ticket)
  interactions: Interaction;
}
