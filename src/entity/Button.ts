import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable
} from "typeorm";
import {Team} from "./Team";
import {ButtonType} from "../models/ticket/ButtonType";

@Entity()
export class Button{

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
      type: "enum",
      enum: ButtonType,
    })
  type: ButtonType;

  @Column()
  name: string;

  @Column()
  form: string;

  @ManyToMany(type => Team, team => team.buttons)
  teams: Team[];
}
