import "reflect-metadata";
import * as express from 'express';
import * as bodyParser from "body-parser";
import {createConnection} from "typeorm";
import {Request, Response} from "express";
import {AppRoutes} from "./Routes";
import * as cors from "cors";


// Create conecction
createConnection().then(async conecction => {

  const app = express();
  app.use(cors())
  app.use(bodyParser.json());

  // register all application routes
    AppRoutes.forEach(route => {
        app[route.method](route.path, (request: Request, response: Response, next: Function) => {
            route.action(request, response)
                .then(() => next)
                .catch(err => next(err));
        });
    });

    app.get('/', (request, response) => {
      response.send('Bienvenido a iTracker Lynx!');
    });

    app.listen(3030, () => {
      console.log("Ready on Port 3030")
    });

}).catch(error => console.log(error))
