import {Request, Response} from "express";
import {verify} from "jsonwebtoken";

export function isLogin(token: string , response:Response ){
  if(!token){
    response.status(401).send({"error": "Para acceder a este sitio debe iniciar sesión"})
    return false;
  }else{

    const verification =  verify(token, "iTrackerLynx", function(err, data){
      if(err){
        response.status(401).send({"error": "Sesión invalida, favor de volver a iniciar"})
        return false;
      }else{
        return true;
      }
    })
    return verification;
  }
}
