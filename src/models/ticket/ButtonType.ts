export enum ButtonType{
  CLOSE = "Close",
  OPEN = "Open",
  DERIVATE = "Derivate",
  REOPEN = "Reopen",
  COMMENT = "Comment",
}
