import {Request, Response} from "express";
import {getManager} from "typeorm";
import {Team} from "../../entity/Team";
import {User} from "../../entity/User";
import {isLogin} from "../managers/SessionManager";



///////////////////////////////
// Obtiene todos los Equipos //
///////////////////////////////
export async function getAllTeams(request: Request, response: Response) {
  const teamRepository = getManager().getRepository(Team);

  const teams = await teamRepository.find();
  response.send(teams);
}



////////////////////////////////////////////
// Obtiene todos los Equipos de u(UserID) //
////////////////////////////////////////////
export async function getAllTeamsOf(request: Request, response: Response) {
  const userRepository = getManager().getRepository(User);
  const username = request.body.username;

  // Recupero el token
  const token = request.headers['lsd-access-token'].toString()

  // Si esta logueado
  if(isLogin(token, response)){
    const users = await userRepository.find({ where: {user:username}, relations: ["teams"] });
    let teamsToSend = []

    // Recorro los equipos
    users.forEach(function(user){
      // Recorro los usuarios
      teamsToSend = user.teams
    });

    response.send(teamsToSend);
  }

}



///////////////////////////////////
//  Agrega un usuario al equipo  //
///////////////////////////////////
export async function addUserToTeam(request: Request, response: Response) {
  const teamRepository = getManager().getRepository(Team);
  const username = request.body.username;
  const idteam = request.body.idteam;
  // Recupero el token
  const token = request.headers['lsd-access-token'].toString()

  // Si el Login esta OK
  if(isLogin(token, response)){
    const userRepository = getManager().getRepository(User);

    // getUser
    const users = await userRepository.find({ where: { user:username }, relations: ["ticketsCreated", "ticketsTaken", "profileTickt", "teams", "interactionsCreate"] });
    // Get Teams
    const teams = await teamRepository.find({ where: { id:idteam } });
    teams.forEach(function (team){
      let user = users.pop()
      user.teams.push(team)

      userRepository.save(user)
      .then(ok =>{
        response.send({info: "Se agrego correctamente"})
      })
      .catch(err =>{
        response.status(401).send({error: "No se pudo agregar usuarios", err: err})
      })
    })
  }
}
