import {Request, Response} from "express";
import axios, { AxiosResponse } from 'axios';
import {getManager} from "typeorm";
import {User} from "../../entity/User";
import {sign} from "jsonwebtoken";



////////////////////////////////
// Obtiene todos los usuarios //
////////////////////////////////
export async function getAllUsers(request: Request, response: Response) {
  const userRepository = getManager().getRepository(User);

  const users = await userRepository.find();
  response.send(users);
}



//////////////////////////
// Autentica un usuario //
//////////////////////////
export async function authUser(request: Request, response: Response) {
  const userRepository = getManager().getRepository(User);
  // Se obtienes datos del post
  const username = request.body.username;
  // const password = request.body.password;

  // Se crea Query para ELDAP
  // const xmlReq = `<execute
  // 	authUser="cn=` + username + `,ou=Personas,ou=Usuarios,o=Telecom"
  // 	authPass="` + password + `">
  // 	<className>UserController</className>
  // 	<methodName>authenticateUser</methodName>
  // 	<parameters>
  // 		<parameter>` + username + `</parameter>
  // 		<parameter>` + password + `</parameter>
  // 	</parameters>
  // 	</execute>`;
  //
  // const xmlConfig = {
  //   headers: {
  //       'Content-Type': 'application/xml'
  //   }
  // }
  //
  // axios.post('https://tuidldap.telecom.com.ar:8443/daas/richclient/Dispatcher', xmlReq, xmlConfig)
  //   .then( (data:AxiosResponse) => {
  //     console.log(data.data);
  //   })
  //   .catch(err => {
  //     console.log(err.data);
  //   });

  const user = await userRepository.find({ where: { user:username } })
  .then(usr => {
    // Chequeo que traiga datos
    if(usr.length == 0){
      response.status(401).send({"error": "No se encontro el usuario, comuniquese con el Administrador" })
    }else{
      // Si los trae genero el Token
      response.send({"token": sign({ user: username, _id: username}, 'iTrackerLynx'),
      "user": user})
    }

  })
  .catch(err=>{
      response.status(401).send({error: "iTracker esta teniendo inconvenientes con DBConnectorin:UserController"})
  })
}



////////////////////////////////
// Retorna Datos de User user //
////////////////////////////////
export async function getUser(request: Request, response: Response){
  const userRepository = getManager().getRepository(User);
  const username = request.body.username;

  const users = await userRepository.find({ where: {user:username}})
  .then(usrs => {
    // Chequeo que traiga datos
    if(usrs.length == 0){
      response.status(401).send({"error": "No se encontro el usuario, comuniquese con el Administrador" })
    }else{
      // Si los trae genero el Token
      response.send({"user": usrs.pop()})
    }

  })
  .catch(err=>{
      response.status(401).send({error: "iTracker esta teniendo inconvenientes con DBConnectorin:UserController para obtener el usuario"})
  })

}
